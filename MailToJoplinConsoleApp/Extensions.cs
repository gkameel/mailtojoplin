﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailToJoplinConsoleApp
{
    static class Extensions
    {
        public static IEnumerable<JoplinFolder> Traverse(this JoplinFolder root)
        {
            var stack = new Stack<JoplinFolder>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                var current = stack.Pop();
                yield return current;
                foreach (var child in current.Children)
                    stack.Push(child);
            }
        }

    }
}
