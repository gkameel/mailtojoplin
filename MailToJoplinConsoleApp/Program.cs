using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace MailToJoplinConsoleApp
{
    class Program
    {
        static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static private string _imapHost = string.Empty;
        static private string ImapHost
        {
            get
            {
                if (String.IsNullOrEmpty(_imapHost))
                {
                    _imapHost = ConfigurationManager.AppSettings["imap.host"];
                }
                return _imapHost;
            }
        }

        static private int ImapPort
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["imap.port"]);
            }
        }

        static bool ImapUseSsl
        {
            get
            {
                return bool.Parse(ConfigurationManager.AppSettings["imap.useSSL"]);
            }
        }

        static string ImapUser
        {
            get
            {
                return ConfigurationManager.AppSettings["imap.user"];
            }
        }
        static string ImapPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["imap.password"];
            }
        }

        static void Main(string[] args)
        {
            //Log.InfoFormat("Is Joplin Online: {0}", JoplinClient.IsOnline());
            if (JoplinClient.IsOnline())
            {
                MailToJoplin();
            }
            else
            {
                Log.Warn("Joplin is NOT online. Please start Joplin with the clipper service");
            }
            Log.Info("Exiting");
        }

        static void MailToJoplin()
        {

            using (var imap = new ImapClient())
            {
                using (var cancel = new CancellationTokenSource())
                {
                    try
                    {
                        //Connect to IMAP server
                        imap.Connect(ImapHost, ImapPort, ImapUseSsl, cancel.Token);
                        Log.DebugFormat("Connected to imap server '{0}'", ImapHost);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("Could not connect to server '{0}:{1}' {2}", ImapHost, ImapPort, (ImapUseSsl) ? "with SSL" : ""), ex);
                        return;
                    }
                    try
                    {
                        // Authenticate
                        imap.AuthenticationMechanisms.Remove("XOAUTH");
                        imap.Authenticate(ImapUser, ImapPassword, cancel.Token);
                        Log.DebugFormat("Authenticated to imap server '{0}'", ImapHost);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("Could not authenticate with IMAP server with user '{0}'", ImapUser), ex);
                        return;
                    }
                    //Get Messages
                    IList<UniqueId> uids;
                    IMailFolder inbox;
                    var successes = new List<UniqueId>();
                    try
                    {
                        inbox = imap.Inbox;
                        inbox.Open(FolderAccess.ReadWrite, cancel.Token);
                        var query = SearchQuery.NotDeleted;
                        uids = inbox.Search(query, cancel.Token);
                        Log.InfoFormat("Found {0} messages ({1} recent)", inbox.Count, inbox.Recent);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Could not get messages from IMAP Inbox", ex);
                        return;
                    }

                    // Message loop: create Note for each message
                    foreach (var uid in uids)
                    {
                        try
                        {
                            var resources = new List<JoplinResource>();
                            var message = inbox.GetMessage(uid, cancel.Token);

                            // Deal with attachments
                            Log.DebugFormat("Message '{0}' ({1}) contains {2} attachments", message.Subject, uid, message.Attachments.Count());
                            foreach (var attachment in message.Attachments)
                            {
                                var fileName = attachment.ContentDisposition?.FileName ?? attachment.ContentType.Name;

                                Log.DebugFormat("Processing attachment '{0}' in message '{1}' ({2})", fileName, message.Subject, uid);

                                using (var stream = new MemoryStream())
                                {
                                    if (attachment is MessagePart)
                                    {
                                        var rfc822 = (MessagePart)attachment;

                                        rfc822.Message.WriteTo(stream);
                                    }
                                    else
                                    {
                                        var part = (MimePart)attachment;

                                        part.Content.DecodeTo(stream);
                                    }
                                    stream.Position = 0;
                                    if (stream.Length > 0)
                                    {
                                        // create Joplin resource
                                        var resource = JoplinClient.CreateResource(stream, fileName);
                                        stream.Close();
                                        if (resource != null)
                                        {
                                            Log.InfoFormat("Created Joplin Resource '{0}' ({1}) for message '{2}' ({3})", resource.title, resource.id, message.Subject, uid);
                                            resources.Add(resource);
                                        }
                                    }
                                }
                            }
                            var body = message.HtmlBody ?? message.TextBody;
                            body = Regex.Replace(body, @"\r\n?|\n", "\n"); //cleaning up
                            body = Regex.Replace(body, @"\n+", "\n"); //cleaning up

                            // inline images
                            body = ConvertInlineImagesToResources(message, body);

                            if (resources.Count > 0)
                            {
                                body += string.Format("\n* * *\n{0}\n", "Attachments:");
                                foreach (var r in resources)
                                {
                                    body += string.Format("[{0}](:/{1})\n", r.title, r.id);
                                }
                            }

                            // Headers 
                            body = string.Format("From: {0}\nTo: {1}\nCc: {2}\nSubject: {3}\nDate: {4}\nPriority: {5}\n* * *\n{6}", message.From, message.To, message.Cc, message.Subject, message.Date, message.Priority, body);

                            // create the note
                            var note = JoplinClient.CreateNote(message.Subject, body);
                            Log.InfoFormat("Created Joplin Note '{0}' ({1}) for message '{2}' ({3})", note.title, note.id, message.Subject, uid);
                            successes.Add(uid);
                        }
                        catch (Exception ex)
                        {
                            Log.Warn(string.Format("Could not create Joplin Note for Imap Message with uid '{0}'", uid), ex);
                        }
                    }
                    // Delete messages that have been converted to Joplin Notes sucessfully
                    foreach (var uid in successes)
                    {
                        try
                        {
                            inbox.AddFlags(uid, MessageFlags.Deleted, true, cancel.Token);
                            Log.DebugFormat("Deleted message with uid '{0}'", uid);
                        }
                        catch (Exception ex)
                        {
                            Log.Debug(string.Format("Could not delete Image message with uid '{0}'", uid), ex);
                        }
                        // Expunge inbox
                        try
                        {
                            inbox.Expunge(cancel.Token);
                            Log.Debug("Inbox expunged");
                        }
                        catch(Exception ex)
                        {
                            Log.Debug("Could not expunge inbox", ex);
                        }
                    }
                }
            }
            return;
        }

        static private string ConvertInlineImagesToResources(MimeMessage newMessage, string bodyHtml)
        {
            if (bodyHtml != null)
            {
                foreach (MimePart att in newMessage.BodyParts)
                {
                    if (att.ContentId != null && att.Content != null && att.ContentType.MediaType == "image" && (bodyHtml.IndexOf("cid:" + att.ContentId) > -1))
                    {
                        var filename = att.ContentDisposition.FileName;

                        using (var stream = new MemoryStream())
                        {
                            att.Content.DecodeTo(stream);
                            stream.Position = 0;
                            var imageAsResource = JoplinClient.CreateResource(stream, filename);
                            stream.Close();
                            bodyHtml = bodyHtml.Replace("cid:" + att.ContentId, string.Format(":/{0}", imageAsResource.id));
                        }
                    }
                }
            }
            return bodyHtml;
        }
    }
}
