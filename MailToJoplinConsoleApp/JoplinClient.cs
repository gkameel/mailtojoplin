﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;

namespace MailToJoplinConsoleApp
{
    static class JoplinClient
    {
        static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static readonly string browserUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36";

        static private string _token = string.Empty;
        private static string Token
        {
            get
            {
                if (String.IsNullOrEmpty(_token))
                {
                    _token = ConfigurationManager.AppSettings["joplin.token"];
                }
                return _token;
            }
        }

        static private string _baseUrl = string.Empty;
        static private string BaseUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_baseUrl))
                {
                    _baseUrl = ConfigurationManager.AppSettings["joplin.baseurl"];
                }
                return _baseUrl;
            }
        }

        static private string _folderTitle = string.Empty;
        static private string FolderTitle
        {
            get
            {
                if (String.IsNullOrEmpty(_folderTitle))
                {
                    _folderTitle = ConfigurationManager.AppSettings["joplin.folder"];
                }
                return _folderTitle;
            }
        }

        static private string _folderId = string.Empty;
        static private string FolderId
        {
            get
            {
                if (String.IsNullOrEmpty(_folderId))
                {
                    _folderId = GetFolderIdByTitle(FolderTitle);
                }
                return _folderId;
            }
        }

        public static bool IsOnline()
        {
            bool isOnline = false;

            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("ping")
                    .SetQueryParam("token", Token);
                var response = url.GetStringAsync().Result;
                if (response == "JoplinClipperServer") isOnline = true;
            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to ping Joplin", ex);
            }
            Log.Debug("IsOnline: " + isOnline);
            return isOnline;
        }

        private static JoplinResource CreateResource(string path, string title = "")
        {
            JoplinResource res = null;
            var file = new FileInfo(path);
            if (string.IsNullOrEmpty(title)) title = file.Name;
            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("resources")
                    .SetQueryParam("token", Token);

                var resource = url.PostMultipartAsync(mp => mp
                        .AddJson("\"props\"", new { title = title })
                        .AddFile("\"data\"", file.FullName)
                        )
                    .ReceiveJson<JoplinResource>()
                    .Result;
                res = resource;
            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to create a resource in Joplin", ex);
            }

            return res;
        }

        public static JoplinResource CreateResource(Stream stream, string fileName)
        {
            JoplinResource resource = null;
            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("resources")
                    .SetQueryParam("token", Token);
                if (string.IsNullOrEmpty(fileName))
                {
                    resource = url.PostMultipartAsync(mp => mp
                            .AddJson("\"props\"", new {})
                            .AddFile("\"data\"", stream, "attachment")
                            )
                        .ReceiveJson<JoplinResource>()
                        .Result;
                }
                else
                {
                    resource = url.PostMultipartAsync(mp => mp
                            .AddJson("\"props\"", new { title = fileName })
                            .AddFile("\"data\"", stream, fileName)
                            )
                        .ReceiveJson<JoplinResource>()
                        .Result;
                }
                Log.DebugFormat("Created Joplin Resource '{0}' ({1})", resource.title, resource.id);
            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to create a resource in Joplin", ex);
            }

            return resource;
        }

        private static string CreateNote()
        {
            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("notes")
                    .SetQueryParam("token", Token);

                var note = url.PostJsonAsync(
                    new JoplinNote
                    {
                        title = "Testing " + DateTime.Now,
                        body = "<b>BODY</b> TEXT\n**bla**",
                        parent_id = FolderId
                    })
                    .ReceiveJson<JoplinNote>()
                    .Result;

            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to create a note in Joplin", ex);
            }

            return "";
        }

        public static JoplinNote CreateNote(string title, string body, IEnumerable<string>files = null)
        {
            JoplinNote note = null;
            var resources = new List<JoplinResource>();

            if (files != null)
            {
                foreach (var file in files)
                {
                    var r = CreateResource(file);
                    if (r != null)
                    {
                        resources.Add(r);
                    }
                }

                if (resources.Count > 0)
                {
                    body += string.Format("\n* * *\n{0}\n", "Attachments:") ;
                    foreach (var r in resources)
                    {
                        body += string.Format("[{0}](:/{1})\n", r.title, r.id);
                    }
                }
            }

            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("notes")
                    .SetQueryParam("token", Token);

                note = url.PostJsonAsync(
                    new JoplinNote
                    {
                        title = title,
                        body = body,
                        parent_id = FolderId
                    })
                    .ReceiveJson<JoplinNote>()
                    .Result;
                Log.DebugFormat("Created Joplin Note '{0}' ({1})", note.title, note.id);

            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to create a note in Joplin", ex);
            }

            return note;
        }

        private static List<JoplinFolder> GetAllFolders()
        {
            var folders = new List<JoplinFolder>();

            try
            {
                var url = BaseUrl
                    .WithHeader("User_Agent", browserUserAgent)
                    .AppendPathSegment("folders")
                    .SetQueryParam("token", Token);
                folders = url
                    .GetJsonAsync<List<JoplinFolder>>()
                    .Result;
            }
            catch (Exception ex)
            {
                Log.Debug("An error occured while trying to get all folders from Joplin", ex);
            }

            return folders;
        }

        private static string GetFolderIdByTitle(string title)
        {
            string id = null;
            var folders = GetAllFolders();
            var list = new List<JoplinFolder>();
            //flatten the tree to list
            foreach (var folder in folders)
            {
                //list.Add(folder);
                list.AddRange(Traverse(folder));
            }
            var firstMatch = list.Where(f => f.Title == title).FirstOrDefault();
            if (firstMatch != null) id = firstMatch.Id;

            return id;
        }

        private static IEnumerable<JoplinFolder> Traverse(this JoplinFolder root)
        {
            var stack = new Stack<JoplinFolder>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                var current = stack.Pop();
                yield return current;
                if (current.Children != null)
                {
                    foreach (var child in current.Children)
                        stack.Push(child);
                }
            }
        }
    }

    public class JoplinFolder
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "children")]
        public List<JoplinFolder> Children { get; set; }
    }

    public class JoplinNote
    {
        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "parent_id")]
        public string parent_id { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string title { get; set; }
        [JsonProperty(PropertyName = "body")]
        public string body { get; set; }
    }

    public class JoplinResource
    {
        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string title { get; set; }
        [JsonProperty(PropertyName = "mime")]
        public string mime { get; set; }
    }
}
